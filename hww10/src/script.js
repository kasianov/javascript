const calc = {
    value1: "",
    value2: "",
    sign: "",
    result_on_display: "",
    memory: ""
}

function clearCalc() {
    calc.value1 = "",
        calc.value2 = "",
        calc.sign = "",
        calc.result_on_display = ""
}
let result = (number_1, number_2) => {
    switch (calc.sign) {
        case "-":
            result_on_display = number_1 - number_2;
            break;
        case "+":
            result_on_display = number_1 + number_2;
            break
        case "*":
            result_on_display = number_1 * number_2;
            break
        case "/":
            if (number_2 === 0) {
                result_on_display = "Error";
            } else {
                result_on_display = number_1 / number_2;
            }
            break
    }
    return result_on_display
}

function show(value, el) {
    el.value = value
}




window.addEventListener("DOMContentLoaded", () => {
    let btn = document.querySelector(".keys")
    let display = document.querySelector(".result")
    let element_m_show = document.createElement('div');

    btn.addEventListener("click", function (e) {
        if (e.target.classList.contains('black')) {

            if (calc.value2 === "" && calc.sign === "") {
                if (display.value == calc.result_on_display && calc.value2 == "" && calc.sign == "") {
                    calc.value1 = "";
                    calc.result_on_display = ""
                }
                calc.value1 += e.target.value;

                if (e.target.value === ".") {
                    if (calc.value1 === 0) {
                        calc.value1 = "0."
                    } else if (calc.value1 != 0) {
                        calc.value1 += "";
                    }
                }
                show(calc.value1, display);
            }

            if (calc.value1 != "" && calc.sign != 0) {
                calc.value2 += e.target.value;

                if (e.target.value === ".") {
                    if (calc.value2 === 0) {
                        calc.value2 = "0."
                    } else if (calc.value2 != 0) {
                        calc.value2 += "";
                    }
                }
                show(calc.value2, display);
            }
            if (e.target.value === 'C') {
                display.value = '';
                clearCalc();
            }
        }

        if (e.target.classList.contains('pink')) {
            calc.sign = e.target.value;
        }

        if (e.target.value === "=") {
            calc.result_on_display = result(parseFloat(calc.value1), parseFloat(calc.value2), calc.sign);
            calc.value1 = calc.result_on_display;
            calc.value2 = "",
                calc.sign = "",
                show(calc.result_on_display, display)
        }
        if (e.target.value === "m+") {
            calc.memory = display.value;
            element_m_show.style.color = 'black';
            element_m_show.style.position = 'relative';
            element_m_show.style.left = '8px';
            element_m_show.style.bottom = '25px';
            element_m_show.style.fontWeight = 'bolder';
            element_m_show.innerHTML = 'm';
            display.after(element_m_show);
        }
        if (e.target.value === "m-") {
            calc.memory = calc.memory - display.value;
        }
        if (e.target.value === "mrc") {
            show(calc.memory, display)
            if (calc.memory === display.value) {
                calc.memory = '';
                element_m_show.remove();
            }
        }
    })
})


