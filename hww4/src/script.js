document.getElementById("row1").innerHTML = `Завдання 1 <p>`
const array = [2,4,6];

function fn (x) {
   return x*2
}

function map (fn, array) {
    let new_array = [];
    for(let i = 0; i < array.length; i++){
        new_array[i] = fn(array[i])
    }
    return new_array
}
document.getElementById("row2").innerHTML =`Результат map (fn, array) ${map (fn, array)} <p>`;

document.getElementById("row3").innerHTML = `Завдання 2 <p>`

let age = parseInt(prompt("Введіть свій вік"));
function checkAge (age){
    let result = (age > 18) ? true : confirm("Батьки дозволили?");
    return result
} 
document.getElementById("row4").innerHTML = `Результат перевірки віку: ${checkAge(age)}`