window.onload = () => {
    const button = document.querySelector('.button')
    button.onclick = () => {
        let input = document.createElement('input')
        button.replaceWith(input)
        input.classList.add("input")
        let draw = document.createElement('button')
        draw.textContent = 'Circles'
        draw.classList.add("draw")
        input.after(draw)
        draw.onclick = () => {
            let diametr = input.value;
            for (i = 0; i < 100; i++) {
                let circle = document.createElement('div')
                document.body.append(circle);
                circle.style.borderRadius = "50%";
                circle.style.display = 'inline-block'
                circle.style.width = `${parseFloat(diametr)}px`;
                circle.style.height = `${parseFloat(diametr)}px`;
                circle.style.backgroundColor = `hsl(${Math.floor(
                    Math.random() * 360
                )}, 50%,50%)`
                circle.onclick = () => {
                    const list = document.getElementById("circle");
                    circle.remove();
                }
            }
        }


    }
}