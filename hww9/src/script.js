let stopWatchScreen = document.querySelector('.container-stopwatch')
let start = document.getElementById("start")
let stop = document.querySelector(".stop")
let reset = document.querySelector(".reset")

let sec = document.querySelector(".sec")
let minut = document.querySelector(".minut")
let hour = document.querySelector(".hour")

let counter1 = 00;
let counter2 = 00;
let counter3 = 00;

const work = () => {
    counter1 += 1
    sec.textContent = counter1
    if (counter1 === 100) {
        counter2++
        counter1 = 00
        minut.textContent = counter2
    } else if (counter2 === 60) {
        counter3++
        counter2 = 00
        hour.textContent = counter3
    }
}



start.onclick = () => {
    stopWatchScreen.classList.remove('green', 'red', 'silver')
    stopWatchScreen.classList.add('green')
    timer = setInterval(work, 1000)
}

stop.onclick = () => {
    stopWatchScreen.classList.remove('green', 'red', 'silver')
    stopWatchScreen.classList.add('red')
    clearInterval(timer);
}

reset.onclick = () => {
    stopWatchScreen.classList.remove('green', 'red', 'silver')
    stopWatchScreen.classList.add('silver');
    clearInterval(timer);
    counter1 = 00;
    counter2 = 00;
    counter3 = 00;
    sec.textContent = counter1
    minut.textContent = counter2
    hour.textContent = counter3
} 

let number = document.querySelector('.number');
let input = document.createElement('input');
input.setAttribute(`placeholder`, `000-000-00-00`);
let save = document.createElement('button');
const pattern = /^\d{3}-\d{3}-\d{2}-\d{2}$/;  
number.append(input)
let send = document.createElement('button');
input.after(send)
send.textContent=("SEND")
//let num = input.value;
send.onclick = () => {
    if (pattern.test(input.value)) {
        input.classList.add("green")
        document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg'
    }
    else {
        let error = document.createElement('div');
        const mis = document.createElement("p");
        mis.textContent = "ERROR";
        document.body.prepend(mis);
        mis.classList.add("red")
        let del = () => {
            mis.remove();
        }
        setTimeout(del, 2000)
    }
} 
let offset = 0;
const sliderLine = document.querySelector('.slider>div:first-child');

const nextSlide = () => {
    offset = offset + 700;
    if (offset > 2100){
        offset = 0;
    }
    sliderLine.style.left = -offset + 'px';
}

setInterval(nextSlide, 2000);
