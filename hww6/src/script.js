
function Human(name, age) {
    this.name = name;
    this.age = age;
}
let people = []
people.push(new Human('Ivan', 4)),
    people.push(new Human('Boris', 66)),
    people.push(new Human('Lola', 21))

function a(a, b) {
    return a.age - b.age;
}
console.log(people.sort(a))

function b(a, b) {
    return b.age - a.age;
} 
