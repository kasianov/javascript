
/* Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища */

class User {
    constructor() {
        this.name = "";
        this.surname = "";
    }
    show() {
        this.user = document.createElement("div");
        document.body.append(this.user);
        this.user.style.border  = "1px solid #000000";
        this.user.style.width  = "250px";
        this.user.textContent = `Ім'я: ${this.name} | Прізвище: ${this.surname}`;

    }
}
const user = new User()
user.name = "Ivan";
user.surname = "Kasianov";
user.show()

/* Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний */

const Ul = document.createElement("ul");
const Li1 = document.createElement("li");
const Li2 = document.createElement("li");
const Li3 = document.createElement("li");
const Li4 = document.createElement("li");

document.body.append(Ul);
Ul.append(Li1, Li2, Li3, Li4);

const e = document.getElementsByTagName("li")[1];
e.previousElementSibling.style.color = "blue";
e.nextElementSibling.style.color = "red";

/* Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки */

const div = document.createElement("div");
document.body.append(div);
div.style.width = "400px";
div.style.height = "400px";
div.style.border = "1px solid #000000";
div.style.textAlign = "center";

/* div.onmousemove = function (e) {
    let a = this.innerHTML = "X:" + e.offsetX + " Y:" + e.offsetY;
    a.style.color = "red"
} */

div.addEventListener("mousemove",(e) => {
    div.innerHTML = `Координата X: ${e.clientX} | Координата Y: ${e.offsetY}`;
});

/* Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута */

const div3 = document.createElement("div");
const button1 = document.createElement("button");
const button2 = document.createElement("button");
const button3 = document.createElement("button");
const button4 = document.createElement("button");

document.body.append(div3);
div3.append(button1, button2, button3, button4);
div3.style.marginTop = "20px"
button1.innerHTML = "button1";
button2.innerHTML = "button2"
button3.innerHTML = "button3"
button4.innerHTML = "button4"

const info = document.createElement("span");
div3.after(info);
div3.addEventListener("click", (e) => {
    info.innerText = (`Кнопка ${e.target.innerHTML} \n`)
    alert(info.innerText)
    console.log(info)
})

/* Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці */

const div4 = document.createElement("div");
document.body.append(div4);
div4.style.width = "150px";
div4.style.height = "150px";
div4.style.backgroundColor = "#10e089";
div4.style.marginTop = "20px"
div4.style.position = "relative"
div4.style.left = "20px"
div4.style.transition = "left 2s"

const mouse = (e) => {
    e.target.style.left = "100px"
}
 
div4.addEventListener("mouseover", mouse)

div4.onmouseout = function () {
    div4.style.left = "20px"
}

/* Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body */

const inputColor = document.createElement("input");
const ColorBtn = document.createElement("button");
div4.after(inputColor);
inputColor.after(ColorBtn)
inputColor.setAttribute("type", "color");
ColorBtn.innerText = "BTN"
ColorBtn.style.marginLeft = "10px"
inputColor.style.marginTop = "20px"


ColorBtn.addEventListener("click", (e) => {
let userChoice = inputColor.value;
document.body.style.backgroundColor = userChoice;
})

/* Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль */

const inputLog = document.createElement("input");
document.body.append(inputLog)
inputLog.setAttribute("placeholder", "Login");
inputLog.style.display = "block";
inputLog.style.marginTop = "20px"
inputLog.style.marginLeft = "10px"

inputLog.addEventListener("input", () => {
    console.log(inputLog.value)
})

/* Створіть поле для введення даних у полі введення даних виведіть текст під полем */ 

const inputLog2 = document.createElement("input");
document.body.append(inputLog2)
inputLog2.setAttribute("placeholder", "Login2");
inputLog2.style.marginTop = "20px"
inputLog2.style.display = "block";
inputLog2.style.marginLeft = "10px"
const text = document.createElement("span");
text.style.marginLeft = "10px"

inputLog2.after(text);

inputLog2.addEventListener("change", () => {
    text.append(`${inputLog2.value} \n`);
    console.log(text)
})