let url = "https://swapi.dev/api/people";
//створюємо запит на сервер за допомогою fetch
const data = fetch(url, { method: "get" });
//робота з запитом, обробка як Promise
let data1 = data.then((rez) => rez.json(), (error) => console.error(error));

data1.then((rez1) => {
	console.log(rez1)
	showPeople(rez1)

})
function showPeople(arr) {
	arr.results.forEach(element => {
		const { name, gender, height, skin_color, birth_year } = element

		const cardBody = document.createElement("div");
		const cardTitle = document.createElement("h3");
		const order = document.createElement("div");
		const cardText = document.createElement("ul");
		const genderLi = document.createElement("li");
		const heightLi = document.createElement("li");
		const skin_colorLi = document.createElement("li");
		const birth_yearLi = document.createElement("li");
		const save = document.createElement("button");


		cardTitle.innerText = name;
		genderLi.innerText = gender;
		heightLi.innerText = height;
		skin_colorLi.innerText = skin_color;
		birth_yearLi.innerText = birth_year;
		cardBody.style.backgroundColor = "#FFB6C1"
		cardBody.style.width = "200px"
		cardBody.style.height = "120px"
		cardBody.style.borderRadius = "5%"
		cardBody.style.border = "1px solid #000000"
		cardBody.style.marginBottom = "10px"
		cardTitle.style.margin = "0 0 0 0"
		cardTitle.style.paddingLeft = "20px"
		cardTitle.style.paddingTop = "5px"
		cardText.style.marginTop = "10px"
		save.style.position = "relative";
		save.style.left = "115px";
		save.style.bottom = "75px";
		save.innerText = "SAVE"


		document.body.append(cardBody);
		cardBody.append(cardTitle, order, save);
		order.append(cardText)
		cardText.append(genderLi, heightLi, skin_colorLi, birth_yearLi)
		save.addEventListener("click", () => {
			localStorage.setItem('peoples', JSON.stringify(arr.results))
	})
	});
}
